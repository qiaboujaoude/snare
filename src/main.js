import Vue from 'vue'
import App from './App'
import axios from 'axios'
import VueAxios from 'vue-axios'
import router from './router'
import Bulma from './assets/sass/main.scss'
import Search from '@/components/Search'
import HeaderTop from '@/components/HeaderTop'
import Loader from '@/components/Loader'
// only import the icons you use to reduce bundle size
// import 'vue-awesome/icons/flag'
// or import all icons if you don't care about bundle size
import 'vue-awesome/icons'
/* Register component with one of 2 methods */
import Icon from 'vue-awesome/components/Icon'

// globally components
Vue.use(VueAxios, axios)
Vue.component('icon', Icon)
Vue.component('search-modal', Search)
Vue.component('header-top', HeaderTop)
Vue.component('loader', Loader)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
