import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Artist from '@/components/Artist'
import Album from '@/components/Album'
// import Debug from '@/components/Debug'

Vue.use(Router)

export default new Router({

  mode: 'history',

  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/artist/:artistName',
      name: 'Artist',
      component: Artist
    },
    {
      path: '/album/:artistName/:albumID',
      name: 'Album',
      component: Album
    }
  ]
})
